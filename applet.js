const Applet = imports.ui.applet;
const PopupMenu = imports.ui.popupMenu;
const GLib = imports.gi.GLib;
const Lang = imports.lang;

const UUID = "alsapplet";
// Logging
function log(message, always=true) {
    if (always)
        global.log("[" + UUID + "]: " + message);
}

function logError(error) {
    global.logError("[" + UUID + "]: " + error)
}

function run(cmd) {
    try {
        let [result, stdout, stderr] = GLib.spawn_command_line_sync(cmd);
        if (stdout !== null) {
            return stdout.toString();
        }
    } catch (error) {
        global.logError(error.message);
    }
}


class MyApplet extends Applet.IconApplet {
    constructor(orientation, panelHeight, instanceId) {
        super(orientation, panelHeight, instanceId);

        this.set_applet_icon_name("audio-headphones");
        this.set_applet_tooltip(_("Enable headphones with alsa and disable speacker"));

        this.menuManager = new PopupMenu.PopupMenuManager(this);
        this.menu = new Applet.AppletPopupMenu(this, orientation);
        this.menuManager.addMenu(this.menu);

        this._contentSection = new PopupMenu.PopupMenuSection();
        this.menu.addMenuItem(this._contentSection);

        this.RadioItem = new PopupMenu.PopupSubMenuMenuItem(_("Audio Mode"));

        let headphoneOnOff = new PopupMenu.PopupSwitchMenuItem(_("Headphone"), false);
        headphoneOnOff.connect("toggled", Lang.bind(this, function (menuItem, state) {
            if (state) {
                run('amixer sset Speaker 0%');
                run('amixer sset Headphone on');
                run('amixer sset Headphone 100%');
            } else {
                run('amixer sset Speaker 100%');
                run('amixer sset Headphone off');
                run('amixer sset Headphone 0%');
            }
        }));
        this.menu.addMenuItem(headphoneOnOff);

        let masterVolume = new PopupMenu.PopupSliderMenuItem(100);
        masterVolume.connect('value-changed', Lang.bind(this, (item, oldVal, newVal)=>{
            // log(`value changed ${oldVal}`)
            run(`amixer sset Master ${parseInt(oldVal*100)}%`);
        }));
        this.menu.addMenuItem(masterVolume);
    }

    on_applet_clicked() {
        this.menu.toggle();
        // run('amixer sset Speaker 0%');
        // run('amixer sset Headphone on');
        // run('amixer sset Headphone 100%');
    }
}

function main(metadata, orientation, panelHeight, instanceId) {
    return new MyApplet(orientation, panelHeight, instanceId);
}
